# Drupal 7 Blank Theme for Opencharity

Version: 1.0

## Summary
This is the drupal theme from scratch base in a theme that i use for projects, include gulp file, sass folder and modular
sass files

## Usage
to compile the sass file run npm install to add the required modules.
after go to scss folder and compile with gulp watch.

### Features

With gulp file for sass compile a responsive menu and all the necesary files to work in responsive


### Suggested Modules
* [Chaos Tools] (http://drupal.org/project/ctools)
* [Views]       (http://drupal.org/project/views)
* [Admin Menu]  (http://drupal.org/project/admin_menu)
* [Libraries]   (http://drupal.org/project/libraries)
* [Responsive Menu]   (http://drupal.org/project/responsive_menus)
* [Slick]   (http://drupal.org/project/slick)
* [bean]   (http://drupal.org/project/bean)

### Changelog

#### Version 1.0

* initial version

### Credits
* [HTML5 Boilerplate](http://html5boilerplate.com)
* [Normalize.css](http://necolas.github.com/normalize.css)
* [SASS / SCSS](http://sass-lang.com/)
* [Gulp](http://gulpjs.com/)
