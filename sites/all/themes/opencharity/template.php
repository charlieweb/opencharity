<?php

/**
 * Implements hook_css_alter().
 * Forces style.css to be included with <link> tag instead of drupals default @import
 * - because browserSync doesn't support @import.
 * Code from: https://www.drupal.org/project/link_css
 */

function opencharity_css_alter(&$css) {


}

function opencharity_preprocess_html(&$vars) {
    // Add font awesome cdn.
 drupal_add_css('//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css', array('type' => 'external'));
 drupal_add_css('https://fonts.googleapis.com/css?family=Open+Sans', array('type' => 'external'));    
}

// Add functions here


?>
