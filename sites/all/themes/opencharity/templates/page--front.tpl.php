

  <header>
    <div class="header-section container">
    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>
    <?php if ($main_menu): ?>
    <nav class="main-nav">
      <?php print theme('links__system_main_menu', array(
        'links' => $main_menu,
        'attributes' => array(
          'id' => 'main-menu',
          'class' => array('links', 'inline', 'clearfix'),
        ),
        'heading' => array(
          'text' => t('Main menu'),
          'level' => 'h2',
          'class' => array('element-invisible'),
        ),
      )); ?>
    </nav>
    <?php endif; ?>
  </div>
  </header>
  <?php if ($page['header']): ?>
    <div class="hero-section">
      <?php print render($page['header']); ?>
    </div>
  <?php endif; ?>
 <?php print $messages; ?>

  <section id="main">
    <?php if ($page['top_region']): ?>
      <div class="page__event">
        <div class="container">
        <?php print render($page['top_region']); ?>
        </div>
      </div>
    <?php endif; ?>
    <?php if ($page['highlighted']): ?>
      <div id="highlighted">
        <div class="container">
          <h2>get involved</h2>
        <?php print render($page['highlighted']); ?>
        </div>
      </div>
    <?php endif; ?>
    <div id="content">
      <div class="container">
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php print render($title_suffix); ?>
      <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
     </div>
    </div>

  </section> <!-- /#main -->
  <div class="section__news">
  <?php if ($page['region_bottom']): ?>
    <div class="region__blog">
      <div class="container__news">
      <?php print render($page['region_bottom']); ?>
      </div>
    </div>
  <?php endif; ?>
 </div>
  <footer>
    <div class="container">
    <?php print render($page['footer']); ?>
    </div>
  </footer>
