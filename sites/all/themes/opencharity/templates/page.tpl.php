	<header>
			<div class="header-section container">
			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>
			<?php print render($page['header']); ?>
			<?php if ($main_menu): ?>
			<nav class="main-nav">
				<?php print theme('links__system_main_menu', array(
					'links' => $main_menu,
					'attributes' => array(
						'id' => 'main-menu',
						'class' => array('links', 'inline', 'clearfix'),
					),
					'heading' => array(
						'text' => t('Main menu'),
						'level' => 'h2',
						'class' => array('element-invisible'),
					),
				)); ?>
			</nav>
			<?php endif; ?>
		</div>
		</header>
   <div class="container">

		<?php if ($breadcrumb): ?>
		  <div id="breadcrumb"><?php print $breadcrumb; ?></div>
		<?php endif; ?>

    	<?php print $messages; ?>

		<section id="main">
			<?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
			<div id="content">
				<a id="main-content"></a>
				<?php print render($title_prefix); ?>
				<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
				<?php print render($title_suffix); ?>
				<?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
				<?php print render($page['help']); ?>
				<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
				<?php print render($page['content']); ?>
				<?php print $feed_icons; ?>
			</div>

			<?php if ($page['sidebar_first']): ?>
				<aside id="sidebar-first">
					<?php print render($page['sidebar_first']); ?>
				</aside>
			<?php endif; ?>

			<?php if ($page['sidebar_second']): ?>
				<aside id="sidebar-second">
					<?php print render($page['sidebar_second']); ?>
				</aside>
			<?php endif; ?>


		</section> <!-- /#main -->
  </div>
		<?php if ($page['region_bottom']): ?>
			<div class="page__blog">
				<?php print render($page['region_bottom']); ?>
			</div>
		<?php endif; ?>
		<footer>
			<div class="container">
			<?php print render($page['footer']); ?>
		  </div>
		</footer>
